package com.vironit.spotifylibrary;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.spotify.android.appremote.api.ConnectionParams;
import com.spotify.android.appremote.api.Connector;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.protocol.client.CallResult;
import com.spotify.protocol.client.Subscription;
import com.spotify.protocol.types.Capabilities;
import com.spotify.protocol.types.PlayerState;
import com.spotify.protocol.types.Repeat;
import com.spotify.protocol.types.Track;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.unity3d.player.UnityPlayer;

import static com.vironit.spotifylibrary.Launcher.mSpotifyAppRemote;


public class MainActivity extends AppCompatActivity {

    public static boolean isPlaying = false;

    private static final String CLIENT_ID = "eecd9a0ba9304c6fb008bafaa31dc33c";
    private static final String REDIRECT_URI = "com.vironit.spotifylibrary://callback";
    public static final int AUTH_TOKEN_REQUEST_CODE = 0x10;

    private boolean isConnected = false;
    private boolean isPaused = false;

    private ProgressBar loadSpinner;
    private Button leftButton, rightButton;

    private final String albumId = "spotify:album:547DJFUYOl2SBYJbo2jZX1";

    private TextView currentTrackArtistNameText;
    private TextView currentTrackAlbumNameText;
    private TextView currentTrackNameText;
    private ImageView albumImageView;

    private boolean canSkipNext;
    private boolean canSkipPrevious;
    private boolean isPremium;

    private Animation animAlpha;

    private boolean isDebug;
    private boolean firstShowDownloadAlert;
    private boolean firstShowLoginAlert;

    private String currentTrackName;
    private String previousTrackName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firstShowDownloadAlert = false;
        firstShowLoginAlert = false;
        Bundle b = getIntent().getExtras();

        if (b != null) {
            isDebug = b.getBoolean("isDebug");
        } else {
            isDebug = false;
        }

        animAlpha = AnimationUtils.loadAnimation(this, R.anim.button_alpha_anim);

        currentTrackArtistNameText = findViewById(R.id.current_track_artist_name);
        currentTrackAlbumNameText = findViewById(R.id.current_track_album_name);
        currentTrackNameText = findViewById(R.id.current_track_name);

        loadSpinner = findViewById(R.id.load_spinner);
        leftButton = findViewById(R.id.left_btn);
        rightButton = findViewById(R.id.right_btn);
        albumImageView = findViewById(R.id.album_image);

        hideUI();

        loadSpinner.getIndeterminateDrawable().setColorFilter(Color.BLACK, android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mSpotifyAppRemote != null) {
            SpotifyAppRemote.disconnect(mSpotifyAppRemote);
        }
        connect(true);
    }

    private void hideUI() {
        leftButton.setVisibility(View.INVISIBLE);
        rightButton.setVisibility(View.INVISIBLE);
        currentTrackArtistNameText.setVisibility(View.INVISIBLE);
    }

    private void showUI() {
        loadSpinner.setVisibility(View.INVISIBLE);
        leftButton.setVisibility(View.VISIBLE);
        rightButton.setVisibility(View.VISIBLE);
        currentTrackArtistNameText.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
       // SpotifyAppRemote.disconnect(mSpotifyAppRemote);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);

            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN:
                    Toast.makeText(this, "Ok !", Toast.LENGTH_LONG).show();
                    connect(false);
                    break;
                // Auth flow returned an error
                case ERROR:
                    Toast.makeText(this, "Error !", Toast.LENGTH_LONG).show();
                    finish();
                    break;
                // Most likely auth flow was cancelled
                default:
                    if (!firstShowLoginAlert) {
                        firstShowLoginAlert = true;
                        Toast.makeText(this, ":(", Toast.LENGTH_LONG).show();
                    } else {
                        finish();
                    }
                    break;
            }
    }

    private void connect(boolean showAuthUI) {
        if (!SpotifyAppRemote.isSpotifyInstalled(this)) {
            if (!firstShowDownloadAlert) {
                firstShowDownloadAlert = true;
                Toast.makeText(this, "Please install Spotify app !", Toast.LENGTH_LONG).show();
                AuthenticationClient.openDownloadSpotifyActivity(this);
            } else {
                finish();
            }
        } else {
            SpotifyAppRemote.connect(
                    this,
                    new ConnectionParams.Builder(CLIENT_ID)
                            .setRedirectUri(REDIRECT_URI)
                            .showAuthView(showAuthUI)
                            .build(),
                    new Connector.ConnectionListener() {
                        @Override
                        public void onConnected(SpotifyAppRemote spotifyAppRemote) {
                            mSpotifyAppRemote = spotifyAppRemote;
                            isConnected = true;
                            mSpotifyAppRemote.getUserApi().getCapabilities().setResultCallback(new CallResult.ResultCallback<Capabilities>() {
                                @Override
                                public void onResult(Capabilities capabilities) {
                                    isPremium = capabilities.canPlayOnDemand;
                                    if(isPremium || isDebug) {
                                        mSpotifyAppRemote.getPlayerApi().subscribeToPlayerState().setEventCallback(new Subscription.EventCallback<PlayerState>() {
                                            @Override
                                            public void onEvent(PlayerState playerState) {
                                                final Track track = playerState.track;
                                                if (track != null) {
                                                    previousTrackName = currentTrackName;
                                                    currentTrackName = track.name;
                                                    if (isPlaying != playerState.isPaused || !track.album.uri.equals(albumId) || !currentTrackName.equals(previousTrackName)) {
                                                        isPlaying = playerState.isPaused;
                                                        Log.d("Unity", "Sending " + String.valueOf(isPlaying));
                                                        JsonObject jsonObject = new JsonObject();
                                                        jsonObject.addProperty("isPlaying", !isPlaying);
                                                        jsonObject.addProperty("albumId", track.album.uri);
                                                        jsonObject.addProperty("albumName", track.album.name);
                                                        jsonObject.addProperty("trackName", track.name);

                                                        UnityPlayer.UnitySendMessage("MusicManager", "ChangeStatus", jsonObject.toString());
                                                    }
                                                    canSkipNext = playerState.playbackRestrictions.canSkipNext;
                                                    canSkipPrevious = playerState.playbackRestrictions.canSkipPrev;
                                                    currentTrackAlbumNameText.setText(track.album.name);
                                                    currentTrackArtistNameText.setText(track.artist.name);
                                                    currentTrackNameText.setText(track.name);
                                                    mSpotifyAppRemote.getImagesApi().getImage(track.imageUri).setResultCallback(new CallResult.ResultCallback<Bitmap>() {
                                                        @Override
                                                        public void onResult(Bitmap bitmap) {
                                                            albumImageView.setImageBitmap(bitmap);
                                                        }
                                                    });
                                                    showUI();
                                                }
                                            }
                                        });
                                        mSpotifyAppRemote.getPlayerApi().setShuffle(false);
                                        mSpotifyAppRemote.getPlayerApi().setRepeat(Repeat.ALL);
                                        mSpotifyAppRemote.getPlayerApi().play(albumId);
                                    } else {
                                        genBuySpotifyPremiumAlert();
                                    }
                                }
                            });

                        }

                        @Override
                        public void onFailure(Throwable error) {
                            login();
                        }
                    });
        }
    }

    private void genBuySpotifyPremiumAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Spotify Premium")
                .setMessage("For listening to music requires a Spotify premium")
                .setIcon(R.drawable.spotify_icon)
                .setCancelable(false)
                .setPositiveButton("Buy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.spotify.com/int/premium")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private AuthenticationRequest getAuthenticationRequest(AuthenticationResponse.Type type) {
        return new AuthenticationRequest.Builder(CLIENT_ID, type, REDIRECT_URI)
                .setShowDialog(false)
                .setScopes(new String[]{"user-read-email"})
                .setCampaign("com-vironit-spotifyplugin")
                .build();
    }

    public void login() {
        final AuthenticationRequest request = getAuthenticationRequest(AuthenticationResponse.Type.TOKEN);
        AuthenticationClient.openLoginActivity(this, AUTH_TOKEN_REQUEST_CODE, request);
    }

    private boolean checkConnection(String toastText) {
        if (isConnected && mSpotifyAppRemote.isConnected()) {

            return true;
        }
        else {
            Toast.makeText(this, toastText, Toast.LENGTH_LONG).show();
            return false;
        }
    }


    private void genNeedPremiumAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Need Spotify Premium !")
        .setMessage(message)
        .setIcon(R.drawable.spotify_icon)
        .setCancelable(true)
        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onPauseButtonClick(View view) {
        view.startAnimation(animAlpha);
        if (isPaused) {
            mSpotifyAppRemote.getPlayerApi().resume();
            isPaused = false;
        } else {
            mSpotifyAppRemote.getPlayerApi().pause();
            isPaused = true;
        }
    }

    public void onLeftButtonClick(View view) {
        view.startAnimation(animAlpha);
        if (canSkipPrevious) {
            if (checkConnection("Not connected")) {
                mSpotifyAppRemote.getPlayerApi().skipPrevious();
            }
        } else {
            genNeedPremiumAlert("To skip previous you need Spotify Premium");
        }
    }

    public  void onRightButtonClick(View view) {
        view.startAnimation(animAlpha);
        if(canSkipNext) {
            if (checkConnection("Not connected")) {
                mSpotifyAppRemote.getPlayerApi().skipNext();
            }
        } else {
            genNeedPremiumAlert("To skip next you need Spotify Premium");
        }
    }
}
