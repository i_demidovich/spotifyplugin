package com.vironit.spotifylibrary;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.unity3d.player.UnityPlayer;

public  class Launcher {
    public static SpotifyAppRemote mSpotifyAppRemote;

    public static void launch(boolean isDebug) {
        Intent nativeIntent = new Intent(UnityPlayer.currentActivity, MainActivity.class);
        Bundle b = new Bundle();
        b.putBoolean("isDebug", isDebug);
        nativeIntent.putExtras(b);
        UnityPlayer.currentActivity.startActivity(nativeIntent);
    }

    public static void next() {
        if (mSpotifyAppRemote != null) {
            mSpotifyAppRemote.getPlayerApi().skipNext();
        }
    }

    public static void previous() {
        if (mSpotifyAppRemote != null) {
            mSpotifyAppRemote.getPlayerApi().skipPrevious();
        }
    }

    public static void pause() {
        if (mSpotifyAppRemote != null) {
            mSpotifyAppRemote.getPlayerApi().pause();
        }
    }

}
